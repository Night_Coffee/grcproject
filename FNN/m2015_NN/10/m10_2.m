 clear all;
x = (0:0.1:10)';
y = sin(2*x)./exp(x/5);
trnData = [x y];
%输入隶属度函数的个数、类型
numMFs = 5;
mfType = 'gbellmf';
in_fis = genfis1(trnData,numMFs,mfType);
%根据给定数据训练初始系统
epoch_n = 20;
dispOpt = zeros(1,4);
out_fis = anfis(trnData,in_fis,20,dispOpt);
%比较ANFIS输出与训练数据
plot(x,y,x,evalfis(x,out_fis));
legend('训练数据','ANFIS输出');

figure;plot(error);
hold on;
plot(chkEr,'r');
