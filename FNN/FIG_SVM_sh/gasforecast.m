%% 清空环境变量
function [predict_low_r_up]=gasforecast
tic;
close all;
clear;
clc;
format compact;
%% 原始数据的提取
load matlabshi.mat;% 提取数据时注意数据文件名称对应否.
% ts = sh_open;
% time = length(ts);
ts_all = sh_open;
sh_open_length= length(ts_all);
predict_low_r_up=zeros(floor((sh_open_length-288)/5),3);
num_circulation=0;
for num_circulation=1 :(sh_open_length-288)/5  %ceil(mod(sh_open_length,288)/5)
    ts=sh_open(1:288+(num_circulation-1)*5);
    time = length(ts);
    str = sprintf( '第  %g 次循环,第 %g次预测',num_circulation,num_circulation);
    disp(str);
% 画出原始上证指数的每日开盘数
% figure;
% plot(ts,'LineWidth',2);
% title('采集数据点','FontSize',12);
% grid on;
% snapnow;
%% 对原始数据进行模糊信息粒化
 
win_num = floor(time/5);
tsx = 1:win_num;
tsx = tsx';
[Low,R,Up]=FIG_D(ts','triangle',win_num);
 
% % 模糊信息粒化可视化图
% figure;
% hold on;
% plot(Low,'b+');
% plot(R,'r*');
% plot(Up,'gx');
% hold off;
% legend('Low','R','Up',2);
% title('模糊信息粒化可视化图','FontSize',12);
% grid on;
% snapnow;
%% 利用SVM对Low进行回归预测
 
% 数据预处理,将Low进行归一化处理
% mapminmax为matlab自带的映射函数
[low,low_ps] = mapminmax(Low);
low_ps.ymin =0;
low_ps.ymax =1;
% 对Low进行归一化
[low,low_ps] = mapminmax(Low,low_ps);
low = low';
snapnow;
 
% 选择回归预测分析中最佳的SVM参数c&g
% 首先进行粗略选择
[bestmse,bestc,bestg] = SVMcgForRegress(low,tsx,-10,10,-10,10,3,1,1,0.1);
 
% 打印粗略选择结果
% disp('打印粗略选择结果');
% str = sprintf( 'SVM parameters for Low:Best Cross Validation MSE = %g Best c = %g Best g = %g',bestmse,bestc,bestg);
% disp(str);
 
% 根据粗略选择的结果图再进行精细选择
[bestmse,bestc,bestg] = SVMcgForRegress(low,tsx,-4,8,-10,10,3,0.5,0.5,0.05);
 
% 打印精细选择结果
% disp('打印精细选择结果');
% str = sprintf( 'SVM parameters for Low:Best Cross Validation MSE = %g Best c = %g Best g = %g',bestmse,bestc,bestg);
% disp(str);
 
% 训练SVM
cmd = ['-c ', num2str(bestc), ' -g ', num2str(bestg) , ' -s 3 -p 0.1'];
low_model = svmtrain(low, tsx, cmd);
 
% 预测
[low_predict,low_mse] = svmpredict(low,tsx,low_model);
low_predict = mapminmax('reverse',low_predict,low_ps);
predict_low = svmpredict(1,win_num+1,low_model);
predict_low = mapminmax('reverse',predict_low,low_ps);
predict_low;
%% 对于Low的回归预测结果分析
% figure;
% error = low_predict - Low';
% plot(error,'ro');
% title('误差(predicted data-original data)','FontSize',12);
% grid on;
% snapnow;
%% 利用SVM对R进行回归预测
% 数据预处理,将R进行归一化处理
% mapminmax为matlab自带的映射函数
[r,r_ps] = mapminmax(R);
r_ps.ymin =0;
r_ps.ymax =1;
% 对R进行归一化
[r,r_ps] = mapminmax(R,r_ps);
r = r';
% snapnow;
 
% 选择回归预测分析中最佳的SVM参数c&g
% 首先进行粗略选择
[bestmse,bestc,bestg] = SVMcgForRegress(r,tsx,-10,10,-10,10,3,1,1,0.1);
% 打印粗略选择结果
% disp('打印粗略选择结果');
% str = sprintf( 'SVM parameters for R:Best Cross Validation MSE = %g Best c = %g Best g = %g',bestmse,bestc,bestg);
% disp(str);
% 根据粗略选择的结果图再进行精细选择
[bestmse,bestc,bestg] = SVMcgForRegress(r,tsx,-4,8,-10,10,3,0.5,0.5,0.05);
% 打印精细选择结果
% disp('打印精细选择结果');
% str = sprintf( 'SVM parameters for R:Best Cross Validation MSE = %g Best c = %g Best g = %g',bestmse,bestc,bestg);
% disp(str);
 
% 训练SVM
cmd = ['-c ', num2str(bestc), ' -g ', num2str(bestg) , ' -s 3 -p 0.1'];
r_model = svmtrain(r, tsx, cmd);
 
% 预测
[r_predict,r_mse] = svmpredict(r,tsx,r_model);
r_predict = mapminmax('reverse',r_predict,r_ps);
predict_r = svmpredict(1,win_num+1,r_model);
predict_r = mapminmax('reverse',predict_r,r_ps);
% predict_r
%% 对于R的回归预测结果分析
% figure;
% error = r_predict - R';
% plot(error,'ro');
% title('误差(predicted data-original data)','FontSize',12);
% grid on;
% snapnow;
%% 利用SVM对Up进行回归预测
 
% 数据预处理,将up进行归一化处理
% mapminmax为matlab自带的映射函数
[up,up_ps] = mapminmax(Up);
up_ps.ymin = 0;
up_ps.ymax = 1;
% 对Up进行归一化
[up,up_ps] = mapminmax(Up,up_ps);
up = up';
% snapnow;
 
% 选择回归预测分析中最佳的SVM参数c&g
% 首先进行粗略选择
[bestmse,bestc,bestg] = SVMcgForRegress(up,tsx,-10,10,-10,10,3,1,1,0.5);
% % 打印粗略选择结果
% disp('打印粗略选择结果');
% str = sprintf( 'SVM parameters for Up:Best Cross Validation MSE = %g Best c = %g Best g = %g',bestmse,bestc,bestg);
% disp(str);
% 根据粗略选择的结果图再进行精细选择
[bestmse,bestc,bestg] = SVMcgForRegress(up,tsx,-4,8,-10,10,3,0.5,0.5,0.2);
% 打印精细选择结果
% disp('打印精细选择结果');
% str = sprintf( 'SVM parameters for Up:Best Cross Validation MSE = %g Best c = %g Best g = %g',bestmse,bestc,bestg);
% disp(str);
 
% 训练SVM
cmd = ['-c ', num2str(bestc), ' -g ', num2str(bestg) , ' -s 3 -p 0.1'];
up_model = svmtrain(up, tsx, cmd);
 
% 预测
[up_predict,up_mse] = svmpredict(up,tsx,up_model);
up_predict = mapminmax('reverse',up_predict,up_ps);
predict_up = svmpredict(1,win_num+1,up_model);
predict_up = mapminmax('reverse',predict_up,up_ps);
% predict_up
% 对于Up的回归预测结果分析
% figure;
% error = up_predict - Up';
% plot(error,'ro');
% title('误差(predicted data-original data)','FontSize',12);
% grid on;
toc;
%snapnow;
predict_low_r_up(num_circulation,:)=[predict_low predict_r predict_up];
end
%% 子函数 SVMcgForRegress.m
function [mse,bestc,bestg] = SVMcgForRegress(train_label,train,cmin,cmax,gmin,gmax,v,cstep,gstep,msestep)
% SVMcgForClass
% about the parameters of SVMcgForRegress
if nargin < 10
    msestep = 0.1;
end
if nargin < 7
    msestep = 0.1;
    v = 3;
    cstep = 1;
    gstep = 1;
end
if nargin < 6
    msestep = 0.1;
    v = 3;
    cstep = 1;
    gstep = 1;
    gmax = 5;
end
if nargin < 5
    msestep = 0.1;
    v = 3;
    cstep = 1;
    gstep = 1;
    gmax = 5;
    gmin = -5;
end
if nargin < 4
    msestep = 0.1;
    v = 3;
    cstep = 1;
    gstep = 1;
    gmax = 5;
    gmin = -5;
    cmax = 5;
end
if nargin < 3
    msestep = 0.1;
    v = 3;
    cstep = 1;
    gstep = 1;
    gmax = 5;
    gmin = -5;
    cmax = 5;
    cmin = -5;
end
% X:c Y:g cg:mse
[X,Y] = meshgrid(cmin:cstep:cmax,gmin:gstep:gmax);
[m,n] = size(X);
cg = zeros(m,n);
% record accuracy with different c & g,and find the best mse with the smallest c
bestc = 0;
bestg = 0;
mse = 10^10;
basenum = 2;
for i = 1:m
    for j = 1:n
        cmd = ['-v ',num2str(v),' -c ',num2str( basenum^X(i,j) ),' -g ',num2str( basenum^Y(i,j) ),' -s 3'];
        cg(i,j) = svmtrain(train_label, train, cmd);
         
        if cg(i,j) < mse
            mse = cg(i,j);
            bestc = basenum^X(i,j);
            bestg = basenum^Y(i,j);
        end
        if ( cg(i,j) == mse && bestc > basenum^X(i,j) )
            mse = cg(i,j);
            bestc = basenum^X(i,j);
            bestg = basenum^Y(i,j);
        end
         
    end
end
 
[cg,ps] = mapminmax(cg);