function y=my_test()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%读取信息系统文件
file = textread('Zoo.txt','%s','delimiter','\n','whitespace','');
[m,n]=size(file);
for i=1:m
    words=strread(file{i},'%s','delimiter',',');
    X{i}=words(2:17);
    D{i}=words(18);
    words=words';
    
end
X=X'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%读取决策系统文件
% file = textread('decision2.txt','%s','delimiter','\n','whitespace','');
% [m,n]=size(file);
% for i=1:m
%     words=strread(file{i},'%s','delimiter',' ');
%     words=words';
%     D{i}=words;    
% end
D=D';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[B,AT]=ind_reduct(X);       %信息系统的约简（基于不可等价关系约简）
ind_AT=ind(X);     %求信息系统的不可等价关系
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%显示约简信息系统
disp('约简后的信息系统为：');
[m,n]=size(B);
for i=1:m
    disp(B{i});
end
%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%决策系统的正域约简
[l,k]=size(X{1});
pos_d=pos(X,D);%求正域
[B,reduct_attr]=pos_reduct(X,D);%基于正域启发式函数对决策系统约简

%将在正域规则下约简过的信息系统B连接决策规则D，使B之变成一决策信息系统
[m,n]=size(B);
for i=1:m
    if(~isequal(B{i},[]))
        B{i}{1,k+1}=D{i}{1};
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%不知道是否可以这么做
%[B,AT]=ind_reduct(B); %将合并的信息系统和决策系统看成又是一个信息系统，并对其约简

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%显示约简决策系统
disp('约简后的决策系统为：');
[m,n]=size(B);
for i=1:m
    disp(B{i});
end        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%决策系统分类
ind_D=ind(D);
[m,n]=size(ind_D);
for i=1:m
    if (~isequal(ind_D{i},[]))
        Low{i}=low(ind_AT,ind_D{i});%求决策规则D在基于不可等价关系ind_AT下的下近似
        Upp{i}=upp(ind_AT,ind_D{i});%求决策规则D在基于不可等价关系ind_AT下的上近似
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%显示确定性分类结果
disp('约简后的决策系统其确定性规则有：');
[m,n]=size(Low);
certern=[];
for i=1:n
    certern=union(certern,Low{i});
end
disp(certern)

disp('约简后的决策系统其不确定性规则有：');
[m,n]=size(Upp);
uncertern=[];
for i=1:n
    uncertern=union(uncertern,setdiff(Upp{i},Low{i}));
end
disp(uncertern)%显示不确定规则

disp('其不确定性规则的可信度为：');
if(~isempty(uncertern))
    Cer=cer(uncertern,ind_AT);
    disp(Cer)  %显示可行度
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%分别求出信息系统和决策系统的核属性
disp('信息系统的核属性为：');
core_ind=core_ind_reduct(X);
disp(core_ind)
disp('决策系统的核属性为：');
core_pos=core_pos_reduct(X,D);
disp(core_pos)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%子函数部分
function y=cer(uncertern,ind_AT)%%%%%%%%%求规则确定性度
[m,n]=size(uncertern);
[m1,n1]=size(ind_AT);
for i=1:n
    for j=1:m1
        if(ismember(uncertern(i),ind_AT{j}))
            y(i)=size(intersect(ind_AT{j},uncertern(i)),2)/size(ind_AT{j},2);%由定义Cer(r)=|[x]nX
        end
    end
end


function core=core_ind_reduct(X)%%%%%%%基于不可等价关系求信息系统核属性
X1=X;%复制一个X
[m,n]=size(X);
[p,k]=size(X{1});
ind_AT=ind(X);%寻找不可等价关系
reduct_attr1=[];    %不可约去的的属性
reduct_attr2=[];    %不可约去的的属性
for i=1:k    %从第一个属性依次检验到最后一个属性
    B=delete_AT(X,i);
    if(isequal(ind_AT,ind(B)))   %若IND(AT-{a}=IND(AT)      
       X=B;
    else
        reduct_attr1=union(reduct_attr1,i); %则寻找到不可约去的属性
    end
end

for i=k:-1:1    %从最后属性依次检验到第一个属性
    B=delete_AT(X1,i);
    if(isequal(ind_AT,ind(B)))   %若IND(AT-{a}=IND(AT)
       X1=B;
    else
        reduct_attr2=union(reduct_attr2,i);%则寻找到不可约去的属性
    end
end
core=intersect(reduct_attr1,reduct_attr2); %核属性即为reduct_attr1,reduct_attr2两个不可约去的属性的交集


function core=core_pos_reduct(X,D)%%%%%%%%%%%%%%基于正域求决策系统核属性
X1=X;%复制一个X
[m,n]=size(X);
[p,k]=size(X{1});
pos_AT=pos(X,D);%正域
reduct_attr1=[];    %可约去的的属性
reduct_attr2=[];    %可约去的的属性
for i=1:k          %从最后属性依次检验到第一个属性
    B=delete_AT(X,i);
    if(isequal(pos_AT,pos(B,D)))   %若Pos(AT-{a}=Pos(AT)      
       X=B;
    else
        reduct_attr1=union(reduct_attr1,i); %则寻找到不可约去的属性
    end   
end

for i=k:-1:1           %从最后属性依次检验到第一个属性
    B=delete_AT(X1,i);
    if(isequal(pos_AT,pos(B,D)))   %若Pos(AT-{a})=Pos(AT)      
       X1=B;
    else
        reduct_attr2=union(reduct_attr2,i);  %则寻找到不可约去的属性
    end   
end
core=intersect(reduct_attr1,reduct_attr2); %核属性即为reduct_attr1,reduct_attr2两个不可约去的属性的交集


function y=delete_AT(X,ATi) %删除X中第i列的属性值
[m,n]=size(X);
[l,k]=size(X{1});
 for i=1:m
     X{i}{ATi}=[];
 end
 y=X;
 
 
function yy=ind(X) %%%%%%%%%%%%寻找不可分辨关系
[m,n]=size(X);
k=1;
ind_AT=cell(m,1);
for i=1:m
        for j=(i+1):m  %潜在问题，如i=m是终止循环，此时若最后一行不为空的话，将漏扫
            if(~isequal(X{i},[]))  %若X{i}不为空
                ind_AT{k}=union(ind_AT{k},i);  %不可等价关系赋初值
                    if(isequal(X{i},X{j}))
                    X{j}=[];       %若X{i}==X{j},则删除X{j}
                    ind_AT{k}=union(ind_AT{k},j); %寻找不可等价关系
                    end
            end        
        end
     k=k+1;
end
if(~isequal(X{m},[]))
    ind_AT{k-1}=m; %假如最后一行不为空
end
yy=ind_AT; %返回不可等价关系


function [C,reduct_attr]=ind_reduct(X)  %基于不可等价关系约简
%%%%%C为约简后的cell数组，reduct_attr为可约去的属性
[m,n]=size(X);
[p,k]=size(X{1});
ind_AT=ind(X);%寻找不可等价关系
reduct_attr=[];    %可约去的的属性
for i=1:k
    B=delete_AT(X,i);
    if(isequal(ind_AT,ind(B)))   %若IND(AT-{a}=IND(AT)
       reduct_attr=union(reduct_attr,i);  %则寻找到可约去的属性
       X=B;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%剔除重复的行
k=1;
for i=1:m
    if(~isequal(ind_AT{i},[]))
        C_i=ind_AT{i,1}(1);
        C{k,1}=X{C_i};       %返回约简后的信息系统
        k=k+1;
    end
end     


function y=low(P,X)  %%%%%%%%%%%%%求下近似
[m,n]=size(P);
Low=[];
for i=1:m   
    if (~isequal(P{i},[])&&all(ismember(P{i},X))==1)%下近似的定义
       Low=union(Low,P{i});
    end
end
y=Low;


function pos_d=pos(X,D)%求决策系统的正域函数
ind_D=ind(D);  %求决策属性D的不可等价关系
[m,n]=size(ind_D);
ind_X=ind(X); %求信息系统属性X的不可等价关系
low=[]; 
for i=1:m
    for j=1:m
        if(~isequal(ind_X{i},[])&&~isequal(ind_D{j},[]))
            if(ismember(ind_X{i},ind_D{j}))
                low=union(low,ind_X{i});  %由性质Pos_AT(d)=low_AT(X1)Ulow_AT(X2)U...
            end
        end
    end
end
pos_d=low;


function [X,reduct_attr]=pos_reduct(X,D)
%%%%%%%%%%%%%%%%%%%%%决策系统正域约简
[m,n]=size(X);
[p,k]=size(X{1});
pos_AT=pos(X,D);%正域
reduct_attr=[];    %可约去的的属性
for i=1:k
    B=delete_AT(X,i);
    if(isequal(pos_AT,pos(B,D)))   %若Pos(AT-{a})=Pos(AT)
       reduct_attr=union(reduct_attr,i);  %则寻找到可约去的属性
       X=B;
    end   
end


function y=upp(P,X)%%%求上近似
[m,n]=size(P);
Upp=[];
for i=1:m
    intersect(P{i},X);
    if (~isequal(P{i},[])&&isempty(intersect(P{i},X))==0)  %P{i}于X相交不为空
        Upp=union(Upp,P{i});
    end
end
y=Upp;
