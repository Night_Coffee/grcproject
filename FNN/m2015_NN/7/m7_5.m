clear all;
n=-6:0.1:6;
a=radbas(n-2.5);  %中心位置向右平移2.5个单位
b=exp(-(n).^2/2);  %除以2，曲线更加“矮胖”
figure;
plot(n,a);
hold on;
plot(n,b,'r*');  %点线
c=diff(a);  %计算a的微分
hold off;
figure;
plot(c,'k');
