function bpgdtrain
  global bpnet;
  clc;
  load german data;
  c=0;
  p=[];
  for i=1:24
     
          c=c+1;
          p(:,c)=data(1:25,i);
      
  end
  t=zeros(2,24);
  t(1:2,16:24)=1;
  t(2,12:15)=1;
  t(1,1)=1;
  t(1,3)=1;
  t(1,5:9)=1;
  t(1,11)=1;
  t(1,1)=1;
  x=ones(25,2);
  x(:,1)=0;
  bpnet=newff(x,[50,2],{'logsig','logsig'},'traingd');
  bpnet.trainParam.show=50;
  bpnet.trainParam.lr=0.2;
  bpnet.trainParam.epochs=20000;
  bpnet.trainParam.goal=0.5e-1;
  [bpnet]=train(bpnet,p,t);
  
