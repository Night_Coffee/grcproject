function draw_Rect
tic;
close all;
clear;
clc;
format compact;
%% 原始数据的提取

% 载入测试数据上证指数(1990.12.19-2009.08.19)
% 数据是一个4579*6的double型的矩阵,每一行表示每一天的上证指数
% 6列分别表示当天上证指数的开盘指数,指数最高值,指数最低值,收盘指数,当日交易量,当日交易额.
% load chapter15_sh.mat;

% 提取数据
% ts = sh_open; % column
load chapter_WineClass.mat
ts = wine(:,1);
time = length(ts);

% 画出原始上证指数的每日开盘数
figure;
plot(1:59,ts(1:59),'*',60:130,ts(60:130),'o',131:178,ts(131:178),'^');
title('上证指数的每日开盘数(1990.12.20-2009.08.19)','FontSize',12);
xlabel('交易日天数(1990.12.19-2009.08.19)','FontSize',12);
ylabel('开盘数','FontSize',12);
grid on;
print -dtiff -r600 original;

snapnow;

%% draw rect
for i =1:2:length(ts)-2
   x = i;
   y = min([ts(i),ts(i+1),ts(i+2)]);
   w = 2;
   h = max([ts(i),ts(i+1),ts(i+2)]) - y;
   rectangle('Position',[x,y,w,h],'LineStyle','--');
end

%% 对原始数据进行模糊信息粒化

win_num = floor(time/3);
tsx = 1:win_num;
tsx = tsx';
[Low,R,Up]=FIG_D(ts','triangle',win_num);

% 模糊信息粒化可视化图
figure;
hold on;
plot(Low,'b+');
plot(R,'r*');
plot(Up,'gx');
hold off;
legend('Low','R','Up',2);
title('模糊信息粒化可视化图','FontSize',12);
xlabel('粒化窗口数目','FontSize',12);
ylabel('粒化值','FontSize',12);
grid on;
print -dtiff -r600 FIGpic;

snapnow;
