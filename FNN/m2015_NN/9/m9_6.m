clear all;
n = -5:0.1:5;
a = satlin(n);      %调用satlin函数
plot(n,a);
axis([-5 5 -1 2]);  %指定横纵坐标的范围
grid on;
title('satlin函数示意图');
