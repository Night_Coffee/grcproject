clear all;
%定义输入变量
P=[0.1 0.8 0.1 0.9;0.2 0.9 0.1 0.8];
%创建自组织竞争神经网络
net=newc(P,2);
%训练神经网络
net=train(net,P);
%网络仿真
Y=sim(net,P)
YC=vec2ind(Y)  %输出结果
