clear all;
p = rand(2,1);   %权值矩阵
a = rand(6,1);   %输入矩阵
w = rand(6,2);  %输出矩阵
pos = hextop(2,3);
d = linkdist(pos);
lp.order_lr = 0.9;  %默认，可省略
lp.order_steps = 1000;
lp.tune_lr = 0.02;
lp.tune_nd = 1;  %默认，可省略
ls = [];   %学习率
[dW,ls] = learnsom(w,p,[],[],a,[],[],[],[],d,lp,ls)  %计算权值变化矩阵
