clear all;
%产生输入、输出数据
numPts=51;    %数据点个数为51
x=linspace(-1,1,numPts);  %输入数据
y=0.5*sin(pi*x)+0.3*sin(3*pi*x)+0.1*sin(5*pi*x);  %输入数据
data=[x',y'];  %整个数据集
trnData=data(1:2:numPts,:);  %训练数据集
chkData=data(2:2:numPts,:);  %检验数据集
%绘制训练和检验数据的分布曲线
subplot(2,2,1);plot(trnData(:,1),trnData(:,2),'ro',chkData(:,1),chkData(:,2),'x');
legend('训练数据','检验数据');
title('训练和检验数据的分布曲线');
xlabel('(1)');
%采用genfis1函数直接由训练数据生成Takagi-Subeno型模糊推理系统
numMFs=5;  %输入隶属度函数的个数
mfType='gbellmf';   %输入隶属度函数的类型
fisMat=genfis1(trnData,numMFs,mfType);  %初始模糊推理系统
%绘制由函数genfis1生成的模糊推理系统的初始输入变量的隶属度函数曲线
subplot(222);[x1,mf]=plotmf(fisMat,'input',1);
plot(x1,mf);
title('系统训练前的隶属度函数');xlabel('(2)');
%根据给定的训练数据，利用函数anfis训练自适应神经模糊系统
epochs=40;   %训练次数为40
trnOpt=[epochs NaN NaN,NaN,NaN];
dispOpt=[];
[Fis,error,stepsize,chkFis,chkEr]=anfis(trnData,fisMat,trnOpt,dispOpt,chkData)
%绘制模糊推理系统由函数anfis训练后的输入变量的隶属度函数曲线
subplot(2,2,3);[x1,mf]=plotmf(Fis,'input',1);
plot(x1,mf);
title('系统训练后的隶属度函数');xlabel('(3)');
%计算训练后神经模糊系统的输出与训练数据的均方根误差trnRMSE
trnOut1=evalfis(trnData(:,1),Fis);   %训练后神经模糊系统的输出
trnOut2=evalfis(trnData(:,1),chkFis);
trnRMSE1=norm(trnOut1-trnData(:,2))/sqrt(length(trnOut1))
trnRMSE2=norm(trnOut2-trnData(:,2))/sqrt(length(trnOut2))
%计算和绘制神经模糊推理系统的输出曲线
anfis_y1=evalfis(x,Fis);
anfis_y2=evalfis(x,chkFis);
subplot(2,2,4);plot(x,y,'-.',x,anfis_y1,'x',x,anfis_y2,'o');
title('函数输出和ANFSI系统输出');
xlabel('(4)');
legend('原函数的输出','ANFIS_1的输出','ANFIS_2的输出')
