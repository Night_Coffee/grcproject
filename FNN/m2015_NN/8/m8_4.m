 clear all;
%simplecluster_dataset是MATLAB自带的用于聚类的简单数据，其中包含了1000个二维向量。
x = simplecluster_dataset;  %载入数据
plot(x(1,:),x(2,:),'mo');
title('原始数据');
%使用8×8拓扑网络进行聚类
net = selforgmap([8 8]);    %创建自组织映射网络
net = train(net,x);         %训练
y = net(x);
classes = vec2ind(y);  
figure;hist(classes,64)  %显示聚类结果
title('聚类结果');
xlabel('类别');ylabel('类别包含的样本数量');

