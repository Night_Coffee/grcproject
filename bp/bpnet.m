    function y=bpnet(sample)
         global bpnet;
         clc;
        load german label;
         
         sample=label(1:25,1);
         a=sim(bpnet,sample);
         a=round(a);
         b=num2str(a);
         c=bin2dec(b');
         y=c-1;