 clear all;
x=-2:0.01:1;
y=2*x.^6+3*x.^5+3*x.^3-2*x.^2;
P=x(1:15:end);
T=y(1:15:end);
%实现不同的spread下广义回归神经网络函数逼近效果
spread=[0.05 0.2 0.4 0.6 0.8 1];   %3组不同的spread值
line_style={'k.-.','r*:','mo-.','bo--','k^-','bx-'};
for i=1:length(spread)
    net=newgrnn(P,T,spread(i));     %创建广义回归神经网络
    A=sim(net,P);
    plot(P,A,line_style{i});        %创建逼近曲线
    hold on;
end
plot(P,T,'o');
legend('spread=0.05','spread=0.2','spread=0.4','spread=0.6','spread=0.8','spread=1','训练数据');
