lear all;
p = [0 0 1 1; 0 1 0 1];
t = [0 0 0 1];
plotpv(p,t)    %绘制输入向量和目标向量
net=newp(minmax(p),1);  %创建一个感知器网络
net.iw{1,1}=[-1.2 -0.5];  %设定权值
net.b{1}=1;  %设定阈值
plotpc(net.iw{1,1},net.b{1})
