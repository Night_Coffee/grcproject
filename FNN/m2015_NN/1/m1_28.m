clear all;
x = 0:pi/10:2*pi;
y1 = sin(x);      %%正弦曲线1
y2 = sin(x-0.25);  %正弦曲线2
y3 = sin(x-0.5);  %正弦曲线3
figure
plot(x,y1,'g',x,y2,'b--o',x,y3,'c*')  %绘图
grid on;  %网格化
