 clear all
% 定义训练样本矢量 
P = [-1:0.05:1];     % P 为输入矢量 
randn('seed',78341223);  % T 为目标矢量 
T = sin(2*pi*P)+0.1*randn(size(P)); 
% 绘制训练样本数据点 
plot(P,T,'+'); 
title('训练样本点');
hold on; 
plot(P,sin(2*pi*P),':');        % 绘制不含噪声的正弦曲线 
title('不含噪声的正弦曲线 ');
% 定义验证样本 
val.P = [-0.975:0.05:0.975];        % 验证样本的输入矢量 
val.T = sin(2*pi*val.P)+0.1*randn(size(val.P));      % 验证样本的目标矢量 
% 创建一个新的前向神经网络 
net=newff(minmax(P),[5,1],{'tansig','purelin'},'traingdx'); 
% 设置训练参数 
net.trainParam.epochs = 500; 
net = init(net); 
% 训练 BP 网络 
[net,tr]=train(net,P,T,[],[],val); 
% 对 BP 网络进行仿真 
A = sim(net,P); 
% 计算仿真误差 
E = T - A; 
MSE=mse(E) 
% 绘制仿真拟合结果曲线 
plot(P,A,P,T,'+',P,sin(2*pi*P),':'); 
title('仿真拟合曲线')
