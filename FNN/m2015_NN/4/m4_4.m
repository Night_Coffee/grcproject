clear all;
% 初始条件
PR=[0 1;0 1;0 1;0 1;0 1;0 1;0 1;0 1; 0 1;0 1;0 1;0 1;0 1;0 1;0 1];%取值范围
S=1;%神经元数目
%创建感知器神经网络
NET=newp(PR,S);
% 训练样本
P=[1 1 1 1 0 1 1 0 1 1 0 1 1 1 1;
     1 1 0 0 1 0 0 1 1 0 1 0 1 1 1;
     1 1 1 1 0 1 0 1 1 1 1 0 1 1 1;
     1 1 1 1 1 1 0 1 1 0 0 1 1 1 1;
     0 1 0 1 1 0 1 1 0 1 1 1 0 1 0;
     1 1 1 1 1 0 0 1 1 0 0 1 1 1 1;
     0 1 1 1 1 0 1 1 1 1 0 1 1 1 1;
     1 1 1 1 0 1 0 1 0 0 1 0 0 1 0;
     1 1 1 1 0 1 1 1 1 1 0 1 1 1 1;
     1 1 1 1 0 1 1 1 1 0 1 1 1 1 0]';
% 目标函数
T=[1 0 1 0 1 0 1 0 1 0];
% 训练感知器
[net,tr]=train(NET,P,T);
% 感知器的权值
W=net.IW{1};
% 感知器的阈值
B=net.b{1};
% 每次训练的步长
epoch1=tr.epoch;
% 每一步训练的误差
perf1=tr.perf;
% 对训练后的感知器进行仿真
test=[1 1 0 1 0 1 1 0 1 1 0 1 1 1 1;
        1 1 0 0 1 0 0 1 1 0 1 1 1 1 1;
        0 1 0 1 1 0 1 1 0 1 1 1 0 1 0;
        1 1 1 1 1 0 0 1 1 0 0 1 1 1 1;]';%实验数据
result=sim(net,test)
