clear all;
P=[0 0 1 1;0 1 0 1];
T=[0 1 1 1];
net=newp(minmax(P),1);
Y1=sim(net,P)
net.trainParam.epochs=25;
net=train(net,P,T);
Y2=sim(net,P)
perf=mae(Y2-T)

while mae(Y2-T)
[net,Y,e]=adapt(net,P,T);
end
Y=sim(net,P)
