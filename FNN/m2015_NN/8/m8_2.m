 clear all;
%iris_dataset是MATLAB自带的用于分类的样本数据，其中包含了150分鸢尾花数据，每人数据用一个4维向量表示。用竞争神经网络将其分为3类，分别是setosa（刚毛鸢尾花）、发versicolor（变色鸢尾花）和virginica（弗吉尼亚鸢尾花）
inputs = iris_dataset;  %载入数据
net = competlayer(6);   %创建竞争网络
net = train(net,inputs); %训练
view(net)              
outputs = net(inputs);   %分类
classes = vec2ind(outputs);  %格式转换
 classes 
c=hist(classes,3)  %每个类别的数量	