clear all;
x = [0 1 2 3 4 5 6 7 8];
t = [0 0.84 0.91 0.14 -0.77 -0.96 -0.28 0.66 0.99];
plot(x,t,'o')       %效果如图3-1所示
net = feedforwardnet(10);  %创建一个两层前馈网络。该网络有一个隐含层有十元。
net = configure(net,x,t);
y1 = net(x)
plot(x,t,'o',x,y1,'x')      %效果如图3-2所示
%对网络进行训练
net = train(net,x,t);   %效果如图3-3所示
y2 = net(x)
plot(x,t,'o',x,y1,'x',x,y2,'*')  %效果如图3-4所示

