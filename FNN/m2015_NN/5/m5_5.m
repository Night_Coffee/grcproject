>> clear all;
x=-5:5;   
y=3*x-7;      %直线方程为y-3x-7
randn('state',2);  %设置种子，便于重复执行
y=y+randn(1,length(y))*1.55;   %加入噪声的直线
plot(x,y,'ro');
P=x;T=y;
net=newlind(P,T);   %用newlind建立线性层
%新的输入样本
new_x=-5:0.2:5;
new_y=sim(net,new_x);  %仿真
hold on;
plot(new_x,new_y);
legend('原始数据点','最小二乘拟合直线');
title('newlind用于最小二乘拟合直线');
disp('线性网络的权值：')
net.iw  
disp('线性网线的阈值：')
net.b

