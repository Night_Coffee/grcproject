%data=[f1,f2,f3,f4];
%data= textread('data', '%f%f%f%f%f','delimiter', ',');
[M,N]=size(data);%数据集为一个M*N的矩阵，其中每一行代表一个样本
    indices=crossvalind('Kfold',data(1:M,N),10);%进行随机分包
    for k=1:10  %交叉验证k=10，10个包轮流作为测试集
        test = (indices == k); %获得test集元素在数据集中对应的单元编号
        train = ~test;%train集元素的编号为非test元素的编号
        train_data=data(train,:);%从数据集中划分出train样本的数据
 train_target=target(:,train);%获得样本集的测试目标，在本例中是实际分类情况
        test_data=data(test,:);%test样本集
 test_target=target(:,test);

 [HammingLoss(1,k),RankingLoss(1,k),OneError(1,k),Coverage(1,k),Average_Precision(1,k),Outputs,Pre_Labels.MLKNN]=MLKNN_algorithm(train_data,train_target,test_data,test_target);%要验证的算法
 end
%上述结果为输出算法MLKNN的几个验证指标及最后一轮验证的输出和结果矩阵，每个指标都是一个k元素的行向量

%%%%%%
[real_train_index, real_vali_index] = crossvalind('HoldOut', size(temp_train_data,2), val_percent/100);
                real_train_data=temp_train_data(:,real_train_index); %训练数据输入
                real_vali_data=temp_train_data(:,real_vali_index); %验证数据
                
                real_train_data_out=temp_train_data_out(:,real_train_index); %训练数据(输出）
                real_vali_data_out=temp_train_data_out(:,real_vali_index); %验证数据（输出）
                
                real_train_data_out=double(real_train_data_out);
                real_vali_data_out=double(real_vali_data_out);
                
                net=newff(real_train_data,double(real_train_data_out),hiddennum);  %新建网络
                %网络参数设置
                net.trainParam.epochs=epochs;
                net.trainParam.lr=lr;
                net.trainParam.goal=goal;
                
                Val.P=real_vali_data;
                Val.T= real_vali_data_out;
                [net,tr]=train(net,real_train_data,real_train_data_out,[],[],Val,[]);%训练
                real_test_data_out=sim(net,temp_test_data);
                
                for i=1:size(real_test_data_out,2)
                    [minT,index]=min(abs(global_TT-real_test_data_out(i)));
                    real_test_data_out(i)=global_TT(index);
                end

