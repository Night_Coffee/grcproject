 clear all;
pos=hextop(8,5);
net=selforgmap([8 5],'topologyFcn','hextop');
plotsomtop(net);
title('神经元位置');
xlabel('第1行的i个神经元位置');
ylabel('第2行的i个神经元位置');
