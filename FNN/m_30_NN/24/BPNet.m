%%清空之前环境变量
clear
close all
clc

%% time in
tic

load data1 input_train output_train input_test output_test
combine_input = [input_train, input_test];
combine_output = [output_train, output_test];
scale = int32(0.8*length(combine_input));
input_train = combine_input(:, 1:scale);
input_test = combine_input(:, scale:end);
output_train = combine_output(:, 1:scale);
output_test = combine_output(:, scale:end);

%输入数据归一化
[inputn,inputps]=mapminmax(input_train,0,1);
[outputn,outputps]=mapminmax(output_train,0,1);
inputn = inputn';
outputn = outputn';
%测试数据归一化
inputn_test=mapminmax('apply',input_test,inputps);
outputn_test=mapminmax('apply',output_test,outputps);

inputn_test = inputn_test';
outputn_test = outputn_test';

[m1,n1]=size(inputn);
[m2,n2]=size(inputn_test);

for h_num=20:20
    %% 网络结构初始化
    in_num=6;
    %h_num=20;
    out_num=1;

    %Xavier权值初始化
    low1 = -sqrt(6/(in_num+h_num));
    high1 = sqrt(6/(in_num+h_num));
    w1 = low1+(high1-low1).*rand(h_num,in_num);
    b1 = zeros(h_num,1);

    low2 = -sqrt(6/(out_num+h_num));
    high2 = sqrt(6/(out_num+h_num));
    w2 = low2+(high2-low2).*rand(h_num,out_num);
    b2 = zeros(out_num,1);

    % w1=rands(h_num,in_num);
    % b1=rands(h_num,1);
    % w2=rands(h_num,out_num);
    % b2=rands(out_num,1);

    w2_1=w2;w2_2=w2_1;
    w1_1=w1;w1_2=w1_1;
    b1_1=b1;b1_2=b1_1;
    b2_1=b2;b2_2=b2_1;

    %学习率
    xite=0.001;
    alfa=0.05;
    max_iter = 100;
    %% 网络训练
    for ii=1:max_iter
        E_train(ii)=0;
        for i=1:1:m1
           %% 网络预测输出 
            x=inputn(i,:);
            % 隐含层输出
            for j=1:1:h_num
                I(j)=x*w1(j,:)'+b1(j);
                Iout(j)=1/(1+exp(-I(j)));
            end
            % 输出层输出
            yn(i)=w2'*Iout'+b2;
            %yn=mapminmax('reverse',yn,inputps);
           %% 权值阀值修正
            %计算误差
            e=outputn(i)-yn(i);     
            E_train(ii)=E_train(ii)+sum(abs(e));
            %break
            %计算权值变化率
            dw2=e*Iout';
            db2=e;

            for j=1:1:h_num
                S=1/(1+exp(-I(j)));
                FI(j)=S*(1-S);
            end      
            for k=1:1:h_num
                for j=1:1:in_num
                    dw1(k,j)=FI(k)'*x(j);
                    db1(k,1)=FI(k)';
                end
            end

            w1=w1_1+xite*dw1;
            b1=b1_1+xite*db1;
            w2=w2_1+xite*dw2;
            b2=b2_1+xite*db2;

            w1_2=w1_1;w1_1=w1;
            w2_2=w2_1;w2_1=w2;
            b1_2=b1_1;b1_1=b1;
            b2_2=b2_1;b2_1=b2;
        end
        % 每个样本的平均误差
        E_train(ii) = E_train(ii) / m1;

        % predict
        E_test(ii)=0;
        for i=1:m2
            %隐含层输出
            for j=1:1:h_num
                I_(j)=inputn_test(i,:)*w1(j,:)'+b1(j);
                Iout_(j)=1/(1+exp(-I_(j)));
            end

            yn_(i)=w2'*Iout_'+b2;
            e_ = outputn_test(i) - yn_(i);
            E_test(ii)=E_test(ii)+sum(abs(e_));
        end
        E_test(ii) = E_test(ii) / m2;
    end
    fg = h_num - 9;
    figure(fg)
    iii = 1:max_iter;
    E_total = E_train + E_test;
    plot(iii,E_train,'r',iii,E_test,'b',iii,E_total,'g');
    title(['Error for h-num=' num2str(h_num)],'fontsize',12);
    xlabel('Iteration','fontsize',12);
    ylabel('Error','fontsize',12);
    legend(['Train error'],['Test error'],['Generalized error']);
    
    % VC dimension and Error
    E_train_error(fg) = sum(E_train) / length(E_train);
    E_test_error(fg) = sum(E_test) / length(E_test);
    E_total_error(fg) = sum(E_total) / length(E_total);
end
% figure(fg+1)
% jjj = 1:h_num-9;
% plot(jjj,E_train_error,'r',jjj,E_test_error,'b',jjj,E_total_error,'g');
% title(['Error for VC dimension'],'fontsize',12);
% xlabel('h_num(VC)','fontsize',12);
% ylabel('Error','fontsize',12);
% legend(['Train error'],['Test error'],['Generalized error']);

%% time out
toc
% Time to use: 15.27s




