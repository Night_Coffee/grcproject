 clear all;
n = -5:0.1:5;
a = satlins(n);   %调用satlins函数
plot(n,a);
axis([-5 5 -2 2]);  %指定横纵坐标的范围
grid on;
title('satlins函数示意图');
