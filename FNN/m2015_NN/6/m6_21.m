clear all;
%使用格式化，使矩阵x1的每一行的最低和最高值映射到默认区间[-1，+1]。
x1 = [1 2 4; 1 1 1; 3 2 2; 0 0 0];
[y1,PS] = mapminmax(x1)
%应用相同的处理设置新值
x2 = [5 2 3; 1 1 1; 6 7 3; 0 0 0];
y2 = mapminmax('apply',x2,PS)
%利用反归一化再次得到X1，Y1处理。
x1_again = mapminmax('reverse',y1,PS)
