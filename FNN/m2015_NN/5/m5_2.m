 clear all;
P={1 2 1 3 3 2};
Pi={1 3};
T={5.0 6.1 4.0 6.0 6.9 8.0};
%应用newlind构建一个网络以满足上面的输入/输出关系和延迟条件
net=newlind(P,T,Pi);
%验证下一个网络的输出
Y=sim(net,P,Pi)
