clear all;
data = [rand(10,1) 10*rand(10,1)-5 rand(10,1)];  %随机给定系统的输入/输出数据
numMFs = [3 7];      %指定每个输入语言变量的隶属度函数的数值
mfType = char('pimf','trimf');   %指定每个输入语言变量的隶属度类型
fismat = genfis1(data,numMFs,mfType);  %生成模糊推理系统
[x,mf] = plotmf(fismat,'input',1);
subplot(2,1,1), plot(x,mf)
xlabel('input 1 (pimf)')
[x,mf] = plotmf(fismat,'input',2);
subplot(2,1,2), plot(x,mf)
xlabel('input 2 (trimf)')
