 clear all;
%输入列向量
P=[-3 -2 -2 0 0 0 0 2 2 3;0 1 -1 2 1 -1 -2 1 -1 0];
%颜色向量
Tc=[1 1 1 2 2 2 2 1 1 1];
plotvec(P,Tc);
title('训练前输入/权值向量');
xlabel('P(1),W(1)');
ylabel('P(2),W(2)');
T=ind2vec(Tc);
%建立LVQ网络
net=newlvq(minmax(P),4,[0.6 0.4],0.1);
%最多训练步数为150
net.trainParam.epochs=150;
net.trainParam.show=Inf;
net=train(net,P,T);
figure;
plotvec(P,Tc);
hold on;
plotvec(net.IW{1}',vec2ind(net.LW{2}),'rp');
title('训练后输入/权重向量');
xlabel('P(1),W(1)');
ylabel('P(2),W(2)');
p=[0 0.5]';
a=vec2ind(net(p))
