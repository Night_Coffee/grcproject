>> clear all;
[x,t] = iris_dataset;   %加载数据，x为输入样本，t为期望输出
net = lvqnet(10);
net.trainParam.epochs = 50;
net = train(net,x,t);	
view(net)
y = net(x);
perf = perform(net,y,t)
classes = vec2ind(y);
