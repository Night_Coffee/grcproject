clear all;
P1={0 -1 1 1 0 -1 1 0 0 1};
T1={0 -1 0 2 1 -1 0 1 0 1};
net=newlin(P1,T1,[0 1],0.01);
Y1=net(P1)
P2={1 0 -1 -1 1 1 1 0 -1};
T2={2 1 01 -2 0 2 2 1 0};
net=newlin(P2,T2,[0 1],0.01);
Y2=net(P2)
net=init(net);
P3=[P1 P2];
T3=[T1 T2];
net.trainParam.epochs=200;
net.trainParam.goal=0.01;
net=train(net,P3,T3);
Y3=net([P1 P2])
