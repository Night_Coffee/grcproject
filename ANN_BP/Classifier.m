
clear all;
close all;
clc
 
[TrainData1 TrainData2 TrainData3 TrainData4 TrainResult] =  textread('iris.data', '%f%f%f%f%f','delimiter', ',');
%载入训练数据及其目标输出
p1 = [TrainData1 TrainData2 TrainData3 TrainData4]';t1=TrainResult';
[pn1,minp,maxp,tn1,mint,maxt]=premnmx(p1,t1);%归一化处理
%net=newff( minmax(pn1),[6,1],{'tansig','purelin'},'traingdm');
%net = newff( minmax(pn1), [6 1] , {'logsig','purelin' } , 'traingdx');
%%错误率平均2%
net = newff(minmax(pn1),[10,1],{'tansig','purelin'},'traingdm'); %错误率平均1%左右
%newff函数：用于构造前向BP网络。其四个输入参数分别表示：
%参数1：输入样本的最大最小值，本程序通过minmax()实现
%参数2：各层神经元个数。本程序设计隐含层有10个神经元，输出有1个神经元
%参数3：各层神经元传递函数。tansig:tan-sigmoid型函数；purelin:纯线性函数；
%参数4：训练函数。trainlm：Levenbery-Marquardt优化法
 
%以下是与函数有关的训练参数设置
net.trainParam.show = 25;
%显示训练迭代过程（NaN表示不显示），缺省值为25。25表示每间隔25次迭代变化1次
net.trainParam.epochs = 500;
%最大训练次数，超过时停止训练。缺省值为100
net.trainParam.goal = 1e-5;
%训练要求精度,即最小均方误差。缺省值为0
%net.trainParam.lr = 0.3 ;
% 学习步长 - traingd,traingdm
% net.trainParam.mc = 0.95; % 动量项系数 - traingdm,traingdx
% net.trainParam.mem_reduc = 10; % 分块计算Hessian矩阵(仅对Levenberg-Marquardt算法有效) 
% net.trainParam.min_grad = 1e-20; % 最小梯度 
% net.trainParam.time = inf; % 最大训练时间

net = train(net,pn1,tn1);%使用归一化后输入样本Pn1和期望输出矩阵tn1开始训练
 
[TestD1 TestD2 TestD3 TestD4 TestR] = textread('bezdekIris.data' , '%f%f%f%f%f','delimiter', ','); 
%载入测试数据及其目标结果
p2=[TestD1 TestD2 TestD3 TestD4]';t2=TestR';
testInput = tramnmx ( p2 , minp, maxp ) ;%测试数据进行归一化
out = sim( net , testInput ) ;%输出测试数据仿真结果
OutData=postmnmx(out,mint,maxt);%输出反归一化
%测试性能,计算输入相应的网络输出
%下面用于评价网络性能
[row,col ] = size(OutData) ;
OData=round(OutData)';%取整函数
E=zeros(size(OData));
cnt = 0 ;
for jj = 1 : col
        if( OData(jj) ~= TestR(jj)) 
        cnt = cnt + 1 ;
    end
end
e=100 * cnt / col;
e1=abs(OutData'-TestR);%误差绝对值
E=mse(OutData'-TestR); %均方误差
%tsse=sse(OutData'-TestR);%均方误差和值
subplot(2,1,1)
plot(OutData','r.');hold on;plot(TestR,'k--'); %测试数据的仿真输出和目标值对比
subplot(2,1,2);plot(e1,'g*');
sprintf('错误率是 %3.3f%%',e)
sprintf('测试的均方误差是 %3.3f%%',E)



