%FIG+SVM to predict trend of the index of Shanghai

load sh_8_19;
TS = sh_open';

figure;
plot(TS);
legend('Source Data(Time Series)');
grid on;

len = length(TS);
win_num = floor(len/5);
[low,R,up]=FIG_D(TS,'triangle',win_num);

% figure;
% plot(1:win_num,low,'k');
% legend('low');
% grid on;
% figure;
% plot(1:win_num,up,'r');
% legend('up');
% grid on;
% figure;
% plot(1:win_num,R);
% legend('R');
% grid on;

[pre_low,acc_low,model_low,ps_low] = SVM_regression(low,100,500,10,10);

[pre_up,acc_up,model_up,ps_up] = SVM_regression(up,100,500,10,10);

[pre_R,acc_R,model_R,ps_R] = SVM_regression(R,100,500,10,10);

prlow = svmpredict(1,win_num+1,model_low);
prlow = mapminmax('reverse',prlow,ps_low)

prup = svmpredict(1,win_num+1,model_up);
prup = mapminmax('reverse',prup,ps_up)

prR = svmpredict(1,win_num+1,model_R);
prR = mapminmax('reverse',prR,ps_R)





