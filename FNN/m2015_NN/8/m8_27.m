 clear all;
P=[-3 -2 -2 0 0 0 0 +2 +2 +3;0 +1 -1 +2 +1 -1 -2 +1 -1 0];
C=[1 1 1 2 2 2 2 1 1 1];
T=ind2vec(C);
i=1;
cla
for i=1:10
    if C(i)==1
        plot(P(1,i),P(2,i),'+')
        hold on
    else
        plot(P(1,i),P(2,i),'rp')
        hold on
    end
end
title('输入向量');
xlabel('P(1)'); ylabel('P(2)');


 net=newlvq(minmax(P),4,[.6 .4],0.1);
%绘制初始网络竞争神经元的权值向量
hold on
W1=net.iw{1};
plot(W1(1,1),W1(1,2),'*');
title('输入/权值向量');
xlabel('P(1),W(1)'); ylabel('P(2),W(2)');


net.trainParam.epochs=150;
net.trainParam.show=Inf;
net=train(net,P,T)         %训练记录如图8-41所示



W1=net.IW{1};
W2=vec2ind(net.LW{2});
i=1;
cla
for i=1:10
    if C(i) == 1
        plot(P(1,i),P(2,i),'p')
        hold on
    else
        plot(P(1,i),P(2,i),'o')
        hold on
    end
end
j=1;
for j=1:4
    if W2(j)==1;
        plot(W1(j,1),W1(j,2),'p','markersize',15);
        hold on;
    else
        plot(W1(j,1),W1(j,2),'o','markersize',15);
        hold on;
    end
end
title('输入/权值向量');
xlabel('P(1),W(1)'); 
ylabel('P(2),W(2)');

p=[0.2;1];
a=vec2ind(sim(net,p))

