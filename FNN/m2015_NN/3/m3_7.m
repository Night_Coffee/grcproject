clear all;
net = perceptron;
net.inputs{1}.size = 2;  %输入大小为2
net.layers{1}.size = 1;  %层数为1
disp('显示初始化网络权重与阈值：')
net.iw{1,1}, net.b{1}
%更改以下这些值：
net.iw{1,1} = [1 2]; 
net.b{1} = 5;
disp('显示更改后的权值与阈值：')
net.iw{1,1}, net.b{1}
%按如下恢复网络的初始值
net = revert(net);
disp('显示恢复网络的权值与阈值：')
net.iw{1,1}, net.b{1}
