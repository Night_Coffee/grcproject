clear all;
x=linspace(-2,2,40);  %等间隔分割点
y=x;
z=x;
[X,Y,Z]=meshgrid(x,y,z);  %网格点化
s=X.^2+Y.^2+Z.^2;  %函数
slice(X,Y,Z,s,[-1,0,1],[-1,0,1],[-1,0,1]);  %切片绘图
colorbar   %颜色条
