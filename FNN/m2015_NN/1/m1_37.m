clear all;
a=input('输入一个年份：');
%如果年份不能被4整除，则不是闰年
if mod(a,4)~=0
    leap=0;
    %如果年份可以被4整除，而且不能被100整除，则是闰年
elseif mod(a,100)~=0
    leap=1;
    %如果年份可以被4整除，并能被100整除，而且能被400整除，则是闰年
elseif mod(a,400)==0
    leap=1;
    %如果年份可以被4整除，并能被100整除，且不能被400整除，则不是闰年
else leap=0;
end
if leap==1
    disp(strcat(num2str(a),'是闰年！！！'));
else
    disp(strcat(num2str(a),'不是闰年！！！'));
end
