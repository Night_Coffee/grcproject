clear all;
P=1:0.5:10;
randn('state',pi);
T=sin(2*P)+randn(1,length(P));  %给正弦函数加噪声
plot(P,T,'ro');
net=newrb(P,T,0,0.6);  %创建径向基网络
test=1:.2:10;
out=sim(net,test);  %仿真
hold on;
plot(test,out,'b-');
legend('输入的数据','拟合的函数');
