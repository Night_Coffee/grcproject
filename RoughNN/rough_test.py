# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 20:31:33 2018

@author: Administrator
"""

import numpy as np
import itertools
import pandas as pd


#function used to flatten list within a list
def flatten(l):
    return eval('[' + repr(l).replace('[', '').replace(']', '') + ']')

#m= np.array([[2,1,3,1],[3,2,1,2],[2,1,3,1],[2,2,3,2],[1,1,4,3],[1,1,2,3],[3,2,1,2],[1,1,4,3],[2,1,3,1],[3,2,1,2]])
#m=np.array([[1,1,1,1],[2,3,2,0],[2,2,3,0],[2,3,4,0],[3,1,1,1],[3,3,5,1],[3,2,3,0],[3,1,5,0]])
m = list()
d = list()
f = open('Zoo.txt','r')
for each in f.readlines():
    eachLine = each.strip().split(',')
    m.append(eachLine[1:-1])
    d.append(eachLine[-1])

ca = np.array(m) #condition attributes
d = np.array(d)#decision attribute

#grouping same element in decision attributes
unique = np.unique(d)
de=[np.where(d == unique_num) for unique_num in unique]
da=[de[item][0] for item in range(len(de))]
print(da)

#primary stage 
quality=[]
for eachxx in range(len(ca[0])): #count for each columns in ca
    lowerap=[]
    for eachrows in range(len(da)): #count for each rows in da(decision attributes)
            xx=ca[:,eachxx]
            xrows=da[eachrows] # get index of each decision
            xnew= [xx[o] for o in xrows] # get value with same decision from each col
            xd = np.delete(xx, xrows) # del gotten value above
            lowerapp=[result for result in xnew if result not in xd]
            lowerap.append(lowerapp)
            
    lowera=[x for x in lowerap if x != []] #used for removing null element in lower approximation
    lower=flatten(lowera)
    quality.append(len(lower)/(len(m)*1.0)) 
colpos=[i for i, j in enumerate(quality) if j == max(quality, key = float)] # get index of maximum value
kl = np.delete(ca, np.s_[colpos],1)                       
ad= [(np.column_stack((ca[:,colpos[i]],kl[:,j]))) for i in range(len(colpos)) for j in range(len(kl[0]))]

#final step
length=[]
for eachad in range(len(ad)):
    #lowerap=[]
    nt=0
    for each in range(len(da)): # for each decision, extract info
        xx=ad[eachad] # have same decision
        xrows=da[each]
        xnew= np.array(list(itertools.compress(xx, [i in xrows for i in range(len(xx))])))
        xd = np.array(list(itertools.compress(xx, [i not in xrows for i in range(len(xx))])))
        alist = list(map(tuple, xnew))
        bset = set(map(tuple, xd))
        mn=np.array([x for x in alist if x not in bset]) # extract the value in selected not in deleted list, computing similar item
        nt=nt+len(mn)
    length.append(nt)
        
#final quality check
quality2=[(length[each]*1.0/len(m)) for each in range(len(length))]
colpos=[i for i, j in enumerate(quality2) if j == max(quality2, key = float)]

#getting the reduced matrix
reduct_matrix= [ad[p] for p in colpos][0]
reduct_shape=[np.shape(ad[q]) for q in colpos]
print(reduct_shape[0])