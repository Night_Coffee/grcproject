clear all;
%创建一个BP网络
net=newff([-5,5],[4,1],{'tansig','purelin'},'trainlm','learngdm','msereg');
p=[-5 -2 0 2 5];
t=[0 1 1 1 0];
y=net(p)
e=t-y   %误差向量
net.performParam.ratio=20/(20+1);  %设置性能参数
perf=msereg(e,net)
