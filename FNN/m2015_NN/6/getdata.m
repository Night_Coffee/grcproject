function [data,label]=getdata(xlsfile)
% [data,label]=getdata('student.xls')
%在file.xls文件中读入身高与体重 
[~,label]=xlsread(xlsfile,1,'B2:B261');
[height,~]=xlsread(xlsfile,'C2:C261');
[weight,~]=xlsread(xlsfile,'D2:D261');
data=[height,weight];
l=zeros(size(label));
for i=1:length(l)
   if label{i}== '男'
       l(i)=1;
   end
end
label=l;

