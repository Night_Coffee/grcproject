clear all;   %清除工作空间中的所有变量
% 加载数据
load stock1      %股价的数据存储于stock1.mat文件中
plot(1:280,stock1);
xlabel('日期');ylabel('股价');

 %清理
clear all;
% 加载数据
load stock1
% 归一化处理
mi=min(stock1);
ma=max(stock1);
stock1=(stock1-mi)/(ma-mi);
%划分训练数据与测试数据：前140个维训练样本，后140个维测试样本
traindata = stock1(1:140);
%训练
P=[];
for i=1:140-5
    P=[P;traindata(i:i+4)];
end
P=P';
T=[traindata(6:140)];  %期望输出
%创建Elman网络
threshold=[0 1;0 1;0 1;0 1;0 1];
net=newelm(threshold,[0,1],[20,1],{'tansig','purelin'});
%开始训练
net.trainParam.epochs=1000;   %设置迭代次数
% 初始化
net=init(net); 
net=train(net,P,T);
%保存训练好的网络
save stock3 net
%使用训练数据测试一次
y=sim(net,P);
error=y-T;
mse(error);
fprintf('error= %f\n', error);
T = T*(ma-mi) + mi;
y = y*(ma-mi) + mi;
plot(6:140,T,'b-',6:140,y,'r-');
title('使用原始数据测试');
legend('真实值','测试结果');


 clear all;    %清除工作空间变量
%加载数据
load stock3   %前面保存的训练好的Elman网络
load stock1
% 归一化处理
mi=min(stock1);
ma=max(stock1);
testdata = stock1(141:280);
testdata=(testdata-mi)/(ma-mi);
%用后140期数据做测试
Pt=[];
for i=1:135
    Pt=[Pt;testdata(i:i+4)];
end
Pt=Pt';
%测试
Yt=sim(net,Pt); 
%根据归一化公式将预测数据还原成股票价格
YYt=Yt*(ma-mi)+mi;
%目标数据-预测数据
figure
plot(146:280, stock1(146:280), 'r',146:280, YYt, 'b');
legend('真实值', '测试结果');
title('股价预测测试');

net=elmannet;
net=newelm(threshold,[0,1],[20,1],{'tansig','purelin'});
