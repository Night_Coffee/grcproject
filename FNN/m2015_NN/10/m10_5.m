clear all;
%定义一个输入P和相应的目标T
x=[1 2 3 4 5 6];
y=3*x;
%利用newlind函数设计一个线性神经网络
net=newlind(x,y)

%输入测试数据
test=[1.5 2.5 3.5 4.5 5.5 6.5];
%使用sim函数测试网络
y=sim(net,test)

%调用gensim函数创建net网络模型
gensim(net,-1)     %其中-1表示将生成一个连续采样网络模块
