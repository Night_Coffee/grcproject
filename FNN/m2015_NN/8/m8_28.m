clear all;
%样本数据
p=[0.9325	1.0000	1.0000	-0.4526	0.3895	1.0000	1.0000	1.0000;...
    -0.4571	-0.2854	-0.9024	-0.9121	-0.0841	1.0000	-0.2871	0.5647;...
    0.5134	0.9413	0.9711	-0.4187	0.2855	0.8546	0.9478	0.9512;...
    0.1545	0.1564	-0.5000	-0.6571	-0.3333	-0.6667	-0.3333	-0.5000;...
    0.1765	0.7648	0.4259	-0.6472	-0.0563	0.1726	0.5151	0.4212;...
    -0.6744	-0.4541	-0.8454	1.0000	-0.8614	-0.6714	-0.6279	-0.6785;...
    0.4647	0.8710	0.0712	-0.7845	-0.2871	0.8915	0.6553	0.6152;...
    0.6818	1.0000	-0.625	-0.8426	-0.6215	-0.1574	1.0000	0.7782];
%newsom建立SOM网络。Minmax(p)取输入的最大最小值。竞争层为6*6=36个神经元
net=newsom(minmax(p),[6 6]);
plotsom(net.layers{1}.positions)
%7次训练的次数
a=[10 30 50 100 200 500 1000];
%随机初始化一个7*8向量
yc=rands(7,8);
%训练次数为10次
net.trainparam.epochs=a(1);
%训练网络和查看分类
net=train(net,p);
y=sim(net,p);
yc(1,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)

%训练次数为30次
net.trainparam.epochs=a(2);
%训练网络和查看分类
net=train(net,p);
y=sim(net,p);
yc(2,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)

%训练次数为50次
net.trainparam.epochs=a(3);
%训练网络和查看分类
net=train(net,p);
y=sim(net,p);
yc(3,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)

%训练次数为100次
net.trainparam.epochs=a(4);
%训练网络和查看分类
net=train(net,p);
y=sim(net,p);
yc(4,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)

%训练次数为200次
net.trainparam.epochs=a(5);
%训练网络和查看分类
net=train(net,p);
y=sim(net,p);
yc(5,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)        
%训练次数为500次
net.trainparam.epochs=a(6);
%训练网络和查看分类
net=train(net,p);        %得到训练记录过程如图8-43所示

y=sim(net,p);
yc(6,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)
%训练次数为1000次          %得到1000次权值向量图，如图8-44所示。
net.trainparam.epochs=a(7);
%训练网络和查看分类
net=train(net,p);
y=sim(net,p);
yc(7,:)=vec2ind(y);
plotsom(net.iw{1,1},net.layers{1}.distances)
title('权向量')
yc

%网络作分类的预测
%测试样本输入
t=[0.9512 1.0000 0.9458 -0.4215 0.4218 0.9511 0.9645 0.8941]';
%用sim来做网络仿真
r=sim(net,t);
%变换函数，将单值向量转换为下标向量
rr=vec2ind(r)
%查看网络拓扑学结构
plotsomtop(net)            %SOM网络拓扑学结构如图8-45所示
title('SOM网络拓扑结构')

 %查看临近神经元直接的距离情况
plotsomnd(net)            %得到临近神经元直接距离如图8-46所示
title('临近神经元直接的距离')

 %查看每个神经元的分类情况
plotsomhits(net,p)              %每个神经元分类情况如图8-47所示
title('每个神经元的分类')
