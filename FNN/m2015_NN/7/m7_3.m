clear all;
P = [1 2 3 4 5 6 7];
Tc = [1 2 3 2 2 3 1];
T = ind2vec(Tc)          %将数据索引转换为向量组
net = newpnn(P,T);      %创建概率神经网络
Y = sim(net,P)           %仿真
Yc = vec2ind(Y)         %将数组矢量转换成数据索引
