% close all;
clear all;
clc;
x = 0:0.1:10;
y = sin(2*x)./exp(x/5);

trnData = [x' y'];
numMFS = 5;
mfType = 'gbellmf';
initFis = genfis1(trnData,numMFS,mfType);
epoch = 200;
fisMat = anfis(trnData,initFis,epoch);
figure(1);
plot(x,y,'-',x,evalfis(x,fisMat),'*');
legend('Training Data','ANFIS Output');
