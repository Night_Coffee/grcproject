function [predict,accuracy,model,ps] = SVM_regression(TS,ymin,ymax,cmax,gmax)

if nargin < 4
    cmax = 10;
    gmax = 10;
end

ts = TS';
[n,m] = size(ts);
figure;
hold on;
grid on;
plot(ts,'LineWidth',2);

time = 1:n;
time = time';

[tsy,ps] = mapminmax(ts');
ps.ymin = ymin;
ps.ymax = ymax;
[tsy,ps] = mapminmax(ts',ps);
ts_again = mapminmax('reverse',tsy,ps);
tsy = tsy';
ts_again = ts_again';
plot(tsy,'b');
plot(ts_again,'r','LineWidth',2);

mse = 10^7;
for log2c = -cmax:cmax,
  for log2g = -gmax:gmax,
    cmd = ['-v 3 -c ', num2str(2^log2c), ' -g ', num2str(2^log2g) , ' -s 3 -p 0.4 -n 0.1'];
    cv = svmtrain(tsy,time,cmd);
    if (cv < mse),
      mse = cv; bestc = 2^log2c; bestg = 2^log2g;
    end
  end
end
fprintf('(best c=%g, g=%g, mse=%g)\n',bestc, bestg, mse);
cmd = ['-c ', num2str(bestc), ' -g ', num2str(bestg) , ' -s 3 -p 0.4 -n 0.1'];
model = svmtrain(tsy, time, cmd);

% model = svmtrain(tsy,time,'-c 1000 -g 0.02 -s 3 -p 0.4 -n 0.1');
[predict,accuracy] = svmpredict(tsy,time,model);
predict = mapminmax('reverse',predict,ps);

plot(predict,'g','LineWidth',2);
